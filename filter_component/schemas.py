from pydantic import BaseModel


class BaseFilter(BaseModel):
    filters: list["BaseFilter"] = []
