class ComponentNotAnnotatedError(Exception):
    def __init__(self, name):
        super().__init__(f"Component <{name}> is not annotated!")


class ComponentAbstractError(Exception):
    def __init__(self, name, method):
        super().__init__(
            f"Component <{name}> is abstract! Method <{name}.{method}> can not be called!"
        )


class ComponentMethodNotImplementedError(Exception):
    def __init__(self, name, method):
        super().__init__(f"Method <{name}.{method}> is not implemented!")


class ComponentFiledRequiredError(Exception):
    def __init__(self, name, filed):
        super().__init__(f"Field <{name}.{filed}> is not passed and required!")
