from logging import getLogger
from abc import ABCMeta
from copy import deepcopy
from typing import Any
from pydantic import BaseModel

from .schemas import BaseFilter
from .exceptions import ComponentMethodNotImplementedError

logger = getLogger(__file__)

_base_class_defined = False


class ModelMetaclass(ABCMeta):

    def __new__(mcs, cls_name: str, bases: tuple[type[Any], ...], namespace: dict[str, Any], **kwargs: Any) -> "type[BaseComponent]":
        global _base_class_defined
        if _base_class_defined and namespace.get("__abstract__") is None:
            namespace["__abstract__"] = False
            cls: type[BaseComponent] = super().__new__(mcs, cls_name, bases, namespace, **kwargs)  # type: ignore
            return cls
        else:
            _base_class_defined = True
            return super().__new__(mcs, cls_name, bases, namespace, **kwargs)  # type: ignore


class BaseComponent(metaclass=ModelMetaclass):
    __slots__ = {"Meta", "__abstract__", "__component_name__", "__component_type__", "__component_annotation__"}

    @classmethod
    @property
    def name(cls) -> str:
        return cls.__component_name__

    @classmethod
    @property
    def type(cls) -> Any:
        return cls.__component_type__

    @classmethod
    def get(cls) -> BaseFilter:
        if not getattr(cls, "__abstract__"):
            _output: BaseFilter = cls._get(deepcopy(cls.__component_annotation__))
        else:
            _output: BaseFilter = BaseFilter()
        _output.filters.extend(f.get() for f in cls.__subclasses__())
        return _output

    @classmethod
    def _get(cls, annotation) -> BaseFilter:
        return annotation

    @classmethod
    def patch(cls, params: BaseModel) -> BaseFilter:
        if not getattr(cls, "__abstract__"):
            _output: BaseFilter = cls._patch(deepcopy(cls.__component_annotation__), getattr(params, cls.__component_name__, None))
        else:
            _output: BaseFilter = BaseFilter()
        _output.filters.extend(f.patch(params) for f in cls.__subclasses__())
        return _output

    @classmethod
    def _patch(cls, annotation: BaseFilter, value: Any) -> BaseFilter:
        return annotation

    @classmethod
    def apply(cls, params: BaseModel) -> Any:
        _output = []
        if not getattr(cls, "__abstract__") and (v := getattr(params, cls.__component_name__, None)) is not None:
            if (o := cls._apply(model=cls.Meta.model, value=v)) is not None:
                match o:
                    case tuple():
                        _output.append(o)
                    case _:
                        _output.append((o, cls.type))
        _output.extend(f for ff in cls.__subclasses__() for f in ff.apply(params))
        return _output

    @classmethod
    def _apply(cls, model, value: Any) -> Any:
        return

    @classmethod
    def autocomplete(cls, field, params: Any) -> Any:
        if not getattr(cls, "__abstract__") and cls.name == field:
            return cls._autocomplete(model=cls.Meta.model, params=params)
        for subclass in cls.__subclasses__():
            if (o := subclass.autocomplete(field, params)) is not None:
                return o

    @classmethod
    def _autocomplete(cls, model, params: Any) -> Any:
        raise ComponentMethodNotImplementedError(cls.__name__, "_autocomplete")
