import logging

from filter_component import BaseComponent, BaseFilter


def test_base_component():
    from typing import Any
    from enum import Enum

    from pydantic import BaseModel

    class FilterData(BaseModel):
        test_filter1: int = 1
        test_filter2: int = 2
        test_filter3: int = 3

    class FilterType(int, Enum):
        Directory = 0
        TimeRange = 1
        DateRange = 2
        DateTimeRange = 3
        Selector = 4
        Checkbox = 5
        IntRange = 6
        FloatRange = 7
        Input = 8

    class FilterSchema(BaseFilter):
        title: str | None = None
        key: str | None = None
        patchable: bool = False
        autocomplete: bool | None = False
        values: Any | None = []
        options: Any | None = []

    class TestComponent0(BaseComponent):
        __abstract__ = True

    class TestComponent1(TestComponent0):
        __component_type__ = 2
        __component_name__ = "test_filter1"
        __component_annotation__: Any = FilterSchema(
            title="TestFilter1",
            key="test_filter1",
            autocomplete=True,
            type=FilterType.Selector,
        )

        class Meta:
            model = None

        @classmethod
        def _apply(cls, model, value: Any) -> Any:
            return value

    class TestComponent2(TestComponent1):
        __component_type__ = 2
        __component_name__ = "test_filter2"
        __component_annotation__: int = FilterSchema(
            title="TestFilter2",
            key=__component_name__,
            autocomplete=True,
            type=FilterType.Selector,
        )

        class Meta:
            model = None

        @classmethod
        def _apply(cls, model, value: Any) -> Any:
            return value

    class TestComponent3(TestComponent1):
        __component_type__ = 2
        __component_name__ = "test_filter3"
        __component_annotation__: int = FilterSchema(
            title="TestFilter3",
            # key=__component_name__,
            autocomplete=True,
            type=FilterType.Selector,
        )

        class Meta:
            model = None

        @classmethod
        def _patch(cls, annotation, value: int):
            return annotation

        @classmethod
        def _apply(cls, model, value: Any) -> Any:
            return value

    logging.info(FilterData.model_fields)

    logging.info(schema := FilterData.model_validate({"test_filter1": 123}))

    from pydantic import BaseModel

    class Test(BaseModel):
        test_filter1: int | None = None
        test_filter2: int | None = None
        test_filter3: int | None = None

    TestComponent0.apply(schema)

    logging.info(FilterData.model_fields)
    logging.info(Test.model_fields)

    logging.info(Test.model_validate({"test_filter3": 123}))

    logging.info(schema := FilterData.model_validate({"test_filter3": 123}))
    logging.info(schema.model_dump())
    logging.info(TestComponent3.get().model_dump())
    logging.info(TestComponent3.patch(schema).model_dump())
    logging.info(TestComponent3.apply(schema))
